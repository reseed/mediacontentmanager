<?php

namespace reseed\mediaContentManager\models;

use Yii;
use reseed\mediaContentManager\helpers\FileInfo;
use reseed\mediaContentManager\behaviors\ArrayFieldBehavior;
use reseed\mediaContentManager\helpers\FileInfoInterface;
use reseed\mediaContentManager\helpers\UploadedFileInfo;
use reseed\mediaContentManager\processors\Builder;
use reseed\mediaContentManager\processors\ImageProcessor;
use reseed\mediaContentManager\processors\Processor;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * Class File
 *
 * @author Ivan <frostealth@frostealth.ru>
 * @since 1.0
 *
 * @property integer $id
 * @property string $name
 * @property string $path
 * @property integer $processing_type
 * @property string $original_name
 * @property integer $size
 * @property string $mime_type
 * @property string $title
 * @property string $description
 * @property array $metadata
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @package reseed\mediaContentManager\models
 */
class File extends ActiveRecord
{
    const PROCESSING_TYPE_PLAIN = 0;
    const PROCESSING_TYPE_IMAGE = 1;

    const SCENARIO_UPDATE = 'update';

    /** @var string|UploadedFile */
    public $file;

    /** @var FileInfoInterface */
    private $fileInfo;

    /** @var Processor */
    private $processor;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file}}';
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @since Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @return array
     */
    public static function getProcessingTypes()
    {
        return [
            self::PROCESSING_TYPE_PLAIN,
            self::PROCESSING_TYPE_IMAGE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            'blameBehavior' => [
                'class' => BlameableBehavior::className(),
            ],
            'arrayFieldBehavior' => [
                'class' => ArrayFieldBehavior::className(),
                'attributes' => ['metadata'],
                'emptyEncodedValue' => null,
                'emptyDecodedValue' => [],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
            [['path'], 'filter', 'filter' => function ($value) {
                return trim($value , '/');
            }],
            [['path'], 'default', 'value' => 'contents/media/files'],
            [['processing_type'], 'in', 'range' => self::getProcessingTypes()],
            [['title'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 255],
            [['title', 'description'], 'default', 'value' => null],
            [['metadata'], 'default', 'value' => []],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::$app->translate->t('id', 'rereca-app'),
            'name' => Yii::$app->translate->t('name', 'rereca-app'),
            'original_name' => Yii::$app->translate->t('original name', 'rereca-app'),
            'title' => Yii::$app->translate->t('title', 'rereca-app'),
            'description' => Yii::$app->translate->t('description', 'rereca-app'),
            'path' => Yii::$app->translate->t('path', 'rereca-app'),
            'processing_type' => Yii::$app->translate->t('processing type', 'rereca-app'),
            'metadata' => Yii::$app->translate->t('metadata', 'rereca-app'),
            'created_at' => Yii::$app->translate->t('created at', 'rereca-app'),
            'updated_at' => Yii::$app->translate->t('updated at', 'rereca-app'),
            'created_by' => Yii::$app->translate->t('created by', 'rereca-app'),
            'updated_by' => Yii::$app->translate->t('updated by', 'rereca-app')
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_UPDATE => ['title', 'description'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->loadProcessor();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!empty($this->file)) {
            $this->fillSystemData();
            $this->loadProcessor();
            $this->getProcessor()->upload($this->getFileInfo()->getFilename());
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $this->getProcessor()->delete();

        parent::afterDelete();
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.05
     * @access public
     *
     * @return Processor|ImageProcessor
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.05
     * @access public
     *
     * @return string
     */
    public function getUrl()
    {
        return call_user_func_array([$this->getProcessor(), 'getUrl'], func_get_args());
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.05
     * @access protected
     */
    protected function fillSystemData()
    {
        $this->name = uniqid() . '.' . $this->getFileInfo()->getExtension();
        $this->original_name = $this->getFileInfo()->getName();
        $this->size = $this->getFileInfo()->getSize();
        $this->mime_type = $this->getFileInfo()->getType();
        $this->processing_type = $this->getProcessingTypeByMime($this->getFileInfo()->getType());
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.05
     * @access protected
     *
     * @param string $mimeType
     *
     * @return int
     */
    protected function getProcessingTypeByMime($mimeType)
    {
        if (preg_match('/image\/(jpe?g|png)/i', $mimeType)) {
            return self::PROCESSING_TYPE_IMAGE;
        }

        return self::PROCESSING_TYPE_PLAIN;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access protected
     *
     * @return FileInfoInterface
     */
    protected function getFileInfo()
    {
        if (empty($this->fileInfo)) {
            if ($this->file instanceof UploadedFile) {
                $this->fileInfo = new UploadedFileInfo($this->file);
            } else {
                $this->fileInfo = new FileInfo($this->file);
            }
        }

        return $this->fileInfo;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.05
     * @access private
     */
    private function loadProcessor()
    {
        $filename = $this->path . '/' . $this->name;
        $this->processor = Builder::build($filename, $this->processing_type);
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => File::find(),
        ]);

        /**
         * Setup sorting attributes
         */
        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'default' => SORT_DESC
                ],
                'original_name' => [
                    'default' => SORT_DESC
                ],
                'created_at' => [
                    'default' => SORT_DESC
                ],
                'name' => [
                    'default' => SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }
}
