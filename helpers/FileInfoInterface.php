<?php

namespace reseed\mediaContentManager\helpers;

/**
 * Interface FileInfoInterface
 *
 * @author Ivan <frostealth@frostealth.ru>
 * @since 1.0
 * 
 * @package reseed\mediaContentManager\helpers
 */
interface FileInfoInterface
{
    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return string
     */
    public function getFilename();

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return string
     */
    public function getName();

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return string
     */
    public function getExtension();

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return int
     */
    public function getSize();

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return string
     */
    public function getType();
}
