<?php

namespace reseed\mediaContentManager\processors;

use reseed\mediaContentManager\models\File;

/**
 * Class Builder
 *
 * @author Ivan <frostealth@frostealth.ru>
 * @since 1.0
 *
 * @package reseed\mediaContentManager\processors
 */
class Builder
{
    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.05
     * @access public
     *
     * @param string $filename
     * @param integer $type
     *
     * @return Processor
     */
    public static function build($filename, $type = null)
    {
        switch ($type) {
            case File::PROCESSING_TYPE_IMAGE:
                $className = ImageProcessor::className();
                break;
            default:
                $className = Processor::className();
        }

        return \Yii::$container->get($className, [$filename]);
    }
}
