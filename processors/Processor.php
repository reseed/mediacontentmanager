<?php

namespace reseed\mediaContentManager\processors;

use frostealth\yii2\components\s3\StorageInterface;
use yii\base\Configurable;

/**
 * Class Processor
 *
 * @author Ivan <frostealth@frostealth.ru>
 * @since 1.0
 *
 * @package reseed\mediaContentManager\processors
 */
class Processor implements Configurable
{
    /** @var StorageInterface */
    protected $storage;

    /** @var string */
    protected $filename;

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.05
     * @access public
     *
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @param string $filename
     * @param StorageInterface $storage
     */
    public function __construct($filename, StorageInterface $storage)
    {
        $this->filename = $filename;
        $this->storage = $storage;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @param string $filename
     */
    public function upload($filename)
    {
        $this->storage->upload(
            $this->getFilename(),
            file_get_contents($filename)
        );
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     */
    public function delete()
    {
        $this->storage->delete($this->getFilename());
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->storage->getCdnUrl($this->getFilename());
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @return string
     */
    protected function getFilename()
    {
        return $this->filename;
    }
}
