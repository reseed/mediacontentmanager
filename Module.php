<?php

namespace reseed\mediaContentManager;

use frostealth\yii2\components\s3\Storage;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 *
 * @author Ivan <frostealth@frostealth.ru>
 * @since 1.0
 *
 * @package reseed\mediaContentManager
 */
class Module extends \yii\base\Module
{
    /** @var array */
    public $s3Config = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->set(Storage::className(), ArrayHelper::merge($this->s3Config, ['class' => Storage::className()]));

        \Yii::$container->set(
            'frostealth\yii2\components\s3\StorageInterface',
            function () {
                return $this->get(Storage::className());
            }
        );
    }
}
