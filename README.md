# Media Content Manager

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download).

Add

```
{
    "type": "git",
    "url": "git@bitbucket.org:reseed/mediacontentmanager.git"
}
```

to the `repositories` section of your `composer.json` and

```
"reseed/media-content-manager": "*"
```

to the `require` section of your `composer.json` file.

Run migrations
`php yii migrate --migrationPath=@reseed/mediaContentManager/migrations`

## Configuration

Add module to `config/main.php` and configure AWS S3 config

```php
'bootstrap' => [
    // ...
    'mediaContentManager',
    // ...
],
'modules' => [
    // ...
    'mediaContentManager' => [
        'class' => \reseed\mediaContentManager\Module::className(),
        's3Config' => [
            'key' => 'your aws s3 key',
            'secret' => 'your aws s3 secret',
            'bucket' => 'your aws s3 bucket name',
            'cdnHostname' => 'http://example.cloudfront.net',
        ],
    ],
    // ...
],
```

## Usage

### How to save data:

```
use reseed\mediaContentManager\models\File;

$model = new File();
$model->setAttributes([
    'path' => 'contents/mail', // path to save in the storage
    'file' => UploadedFile::getInstanceByName('file'), // or 'path/to/local/file.ext'
]);
$model->save();
```

### How to obtain file url:

```
use reseed\mediaContentManager\models\File;

$model = File::findOne($id);
$url = $model->getUrl();
```

### How to obtain URL for the images (jpeg, jpg, png file formats)

```
use reseed\mediaContentManager\models\File;
use reseed\mediaContentManager\processors\ImageProcessor;

$model = File::findOne($id);
$url = $model->getUrl(ImageProcessor::SIZE_TYPE_SMALL);
```

### How to edit file info:

- via url `/mediaContentManager/files/update/{$file_id}`
- via model:

```
use reseed\mediaContentManager\models\File;

$model = File::findOne($id);
$model->setScenario(File::SCENARIO_UPDATE);
$model->setAttributes([
    'title' => $new_title,
    'description' => $new_description,
    'metadata' => [
        // ...
    ],
]);
$model->save();
```

### How to view file info:

По url: `/mediaContentManager/files/view/{$file_id}`

### How to use `FileBehavior`

Record.php
```
/**
 * Class Record
 *
 * @property int $id
 * @property int $file_id
 *
 * @property File $file
 */
class Record extends ActiveRecord
{
    public $attachment;

    public function behaviors()
    {
        return [
            'fileBehavior' => [
                'class' => FileBehavior::className(),
                'attribute' => 'attachment',
                'path' => 'contents/mail', // or closure
                'relationName' => 'file',
            ],
        ];
    }
    
    public function rules()
    {
        return [
            [['attachment'], 'file'],
        ];
    }
    
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}
```

```
$model = new Record();
$model->setAttribute('attachment', UploadedFile::getInstance($model, 'attachment'));
$model->save();
```

### How to use `PivotFileBehavior`

RelRecordFile.php
```
/**
 * Class RelRecordFile
 * 
 * @property int $id
 * @property int $record_id
 * @property int $file_id
 */
class RelRecordFile extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%rel_record_file}}';
    }
}
```

Record.php
```
use reseed\mediaContentManager\behaviors\PivotFileBehavior

/**
 * Class Record
 * 
 * @property int $id
 */
class Record extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%record}}';
    }
    
    public function behaviors()
    {
        return [
            'fileBehavior' => [
                'class' => PivotFileBehavior::className(),
                'pivotClassName' => RelRecordFile::className(),
                'pivotLink' => ['record_id' => 'id'],
                'link' => ['id' => 'file_id'],
            ],
        ];
    }
    
    public function getFileBehavior()
    {
        return $this->getBehavior('fileBehavior');
    }
}
```

```
$record = Record::findOne($id);
$files = $record->getFilesBehavior()->files;
foreach ($files as $file) {
    echo Html::link($file->original_name, $file->getUrl());
}
```

## API

### reseed\mediaContentManager\models\File

#### Properties:

- $id (readonly)
- $original_name (readonly) - original file name with extension
- $size (readonly) - size of the file in bytes
- $mime_type (readonly)
- $title - file title
- $description - file description
- $metadata - other info about the file
- $path (only when creating) - path to save in the storage
- $name (readonly) - file name in the storage
- $created_by
- $updated_by
- $created_at
- $updated_at

#### Methods:

- getUrl()
- getProcessor()

### reseed\mediaContentManager\processors\ImageProcessor

Used for images only(jpeg, jpg, png).

#### Constants:

- SIZE_TYPE_SMALL
- SIZE_TYPE_MEDIUM
- SIZE_TYPE_LARGE

#### Methods:

- getSizeTypes()
- getUrl($size_type = null)

### reseed\mediaContentManager\behaviors\PivotFileBehavior

used for Record - File relation

#### Properties

- $pivotClassName - class name of pivot model
- $pivotLink
- $pivotOnCondition
- $link

#### Methods:

- getPivot()
- getFiles()