<?php
use yii\grid\GridView;
use yii\grid\ActionColumn;
use reseed\reWidgets\rebox\ReBox;
use reseed\mediaContentManager\models\File;
/**
 * @var \yii\web\View $this
 */

$this->title = Yii::t('mediaContentManager', 'Media Content Manager');
$this->params['breadcrumbs'] = [$this->title];
?>

<div>
    <?php
        ReBox::begin([
            'header' => [
                'options' => [
                    'title' => $this->title
                ],
                'icon' => [
                    'name' => 'table',
                ],
            ],
        ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'original_name',
            'created_at',
            [
                'attribute' => 'name',
                'label' => Yii::$app->translate->t('url', 'rereca-app'),
                'value' => function (File $model) {
                    return $model->getUrl();
                }
            ],
            [
                'class' => ActionColumn::className(),
                'controller' => 'files'
            ]
        ],
    ]) ?>
    
    <?php ReBox::end(); ?>
</div>
